package com.example.quiz

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.math.ln

class MainActivity : AppCompatActivity() {

    private val users= mutableListOf<MyContext>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init()
    {

       var  fname=firstname.text
       var  lname=lastname.text
       var  mail=Email.text
       var  ag=Age.text

        adduser.setOnClickListener {

            if(fname.isNullOrEmpty() || lname.isNullOrEmpty() || ag==null || mail.isNullOrEmpty()){
                Toast.makeText(this,"all fields are required",Toast.LENGTH_SHORT).show()
            }
            else if(!isEmailValid(mail.toString()))  Toast.makeText(this,"ease enter correct E-MAIL adress",Toast.LENGTH_SHORT).show()

            else{
                if(chechuser(mail.toString())) Toast.makeText(this,"this User already exists",Toast.LENGTH_SHORT).show()
              else {

                    var  cont =MyContext(firstName = fname.toString(),lastName = lname.toString(),age = ag.toString().toInt(),email = mail.toString())
                    users.add(cont)
                    Toast.makeText(this,"User added successfully",Toast.LENGTH_SHORT).show()
                    //firstname.text.clear()
                }

            }
        }

       removeuser.setOnClickListener {



           if(fname.isNullOrEmpty() || lname.isNullOrEmpty() || ag==null || mail.isNullOrEmpty()){
                Toast.makeText(this,"all fields are required",Toast.LENGTH_SHORT).show()
           }
             else if(!isEmailValid(mail.toString()))  Toast.makeText(this,"please enter correct E-MAIL adress",Toast.LENGTH_SHORT).show()
            else {
               removeUser(mail.toString())
                 Toast.makeText(this,"User was deleted",Toast.LENGTH_SHORT).show()
            }

        }



    }

    fun isEmailValid(email: String): Boolean {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }
    fun chechuser(mail:String):Boolean{

        for(index in users){
            if(index.email == mail) return  true

        }
        return  false
    }
    fun removeUser(mail:String){
        for(index in users){
            if(index.email==mail){
                users.remove(index)
            }
        }
    }
}
